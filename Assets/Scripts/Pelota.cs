﻿using UnityEngine;
using System.Collections;

public class Pelota : MonoBehaviour
{
    // Movement Speed
    public float speed = 100.0f;

    // Use this for initialization
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.up * speed;
    }
}